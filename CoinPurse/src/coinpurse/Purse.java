package coinpurse; 
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
/**
 *  A coin purse contains coins.
 *  You can insert coins, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  coins to remove.
 *  
 *  @author your mind
 */
public class Purse {
    /** Collection of coins in the purse. */
	
    //TODO declare a List of coins
    private List<Coin> coins;
    /** Capacity is maximum NUMBER of coins the purse can hold.
     *  Capacity is set when the purse is created.
     */
    private int capacity;
    
    /** 
     *  Create a purse with a specified capacity.
     *  @param capacity is maximum number of coins you can put in purse.
     */
    public Purse( int capacity ) {
    	this.capacity = capacity;
    	coins = new ArrayList<Coin>();
	//TODO initialize the attributes of purse
    }

    /**
     * Count and return the number of coins in the purse.
     * This is the number of coins, not their value.
     * @return the number of coins in the purse
     */
    public int count() { 
    	int count = 0;
    	for(int a = 0;a<coins.size();a++){
    		count++;
    	}
    	return count; 
    }
    
    /** 
     *  Get the total value of all items in the purse.
     *  @return the total value of items in the purse.
     */
    public double getBalance() {
    	double balance = 0;
    	for(int a = 0;a<coins.size();a++){
    		balance += coins.get(a).getValue();
    	}
    	return balance; 
    }

    /**
     * Return the capacity of the coin purse.
     * @return the capacity
     */
    public int getCapacity() { return this.capacity; }
    
    /** 
     *  Test whether the purse is full.
     *  The purse is full if number of items in purse equals
     *  or greater than the purse capacity.
     *  @return true if purse is full.
     */
    public boolean isFull() {	
       if(this.capacity == coins.size()){
        return true;
       }
       else{
       return false;
       }
    }

    /** 
     * Insert a coin into the purse.
     * The coin is only inserted if the purse has space for it
     * and the coin has positive value.  No worthless coins!
     * @param coin is a Coin object to insert into purse
     * @return true if coin inserted, false if can't insert
     */
    public boolean insert( Coin coin ) {
    	if(coin.getValue()<=0){
    		return false;
    	}
    	if(isFull()){
    		return false;
    	}
    	else{
    		coins.add(coin);
    		return true;
    	}
    }
    
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of Coins withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @return array of Coin objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
    public Coin[] withdraw(double amount) {
    	if(amount <=0){
    		return null;
    	}
        if (this.getBalance()<amount){
        	return null;
        }
        Collections.sort(coins);
        double test = 0;
      List<Coin> fakePurse = new ArrayList<Coin>();
      fakePurse.addAll(coins);
        List<Coin> CoinList = new ArrayList<Coin>();
        for(int a = fakePurse.size()-1;a>=0;a--){
        	test += fakePurse.get(a).getValue();
        	CoinList.add(fakePurse.get(a));
        	coins.remove(a);
        	if(test > amount){
        		test -= fakePurse.get(a).getValue();
            	CoinList.remove(CoinList.size()-1);
            	coins.add(fakePurse.get(a));
        	}
        }
        Coin[] reCoinList = new Coin[CoinList.size()];
        for(int b = 0;b<CoinList.size();b++){
        	reCoinList[b] = CoinList.get(b);
        }
        if(reCoinList.length==0){
        	return null;
        }
        else{
        	double test2 = 0;
        	for(int v = 0;v<reCoinList.length;v++){
        		test2 += reCoinList[v].getValue();
        	}
        	if(amount-test2 == 0){
        		return reCoinList;
        	}
        	else{
        		for(int q = 0;q<reCoinList.length;q++){
        			coins.add(reCoinList[q]);
        		}
        		return null;
        	}
        	
        }
	   /*
		* One solution is to start from the most valuable coin
		* in the purse and take any coin that maybe used for
		* withdraw.
		* Since you don't know if withdraw is going to succeed, 
		* don't actually withdraw the coins from the purse yet.
		* Instead, create a temporary list.
		* Each time you see a coin that you want to withdraw,
		* add it to the temporary list and deduct the value
		* from amount. (This is called a "Greedy Algorithm".)
		* Or, if you don't like changing the amount parameter,
		* use a local total to keep track of amount withdrawn so far.
		* 
		* If amount is reduced to zero (or tempTotal == amount), 
		* then you are done.
		* Now you can withdraw the coins from the purse.
		* NOTE: Don't use list.removeAll(templist) for this
		* becuase removeAll removes *all* coins from list that
		* are equal (using Coin.equals) to something in templist.
		* Instead, use a loop over templist
		* and remove coins one-by-one.		
		*/
		
//TODO code for withdraw

		// Did we get the full amount?

		// Success.
		// Since this method returns an array of Coin,
		// create an array of the correct size and copy
		// the Coins to withdraw into the array.
        //TODO replace this with real code
	}
  
    /** 
     * toString returns a string description of the purse contents.
     * It can return whatever is a useful description.
     */
    public String toString() {
        String str =String.format("%d coins with value %.1f", this.count(),this.getBalance());
    	return str;
    }

}
//TODO remove the TODO comments after you complete them.
//TODO When you are finished, there should not be any TODO. Including this one.
