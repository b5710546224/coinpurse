package coinpurse;
 /**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author mind
 */
public class Coin implements Comparable<Coin> {
    /** Value of the coin */
    private double value;
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value ) {
        this.value = value;
    }
    /** 
     * method for getting the value of coin. 
     * @return the value
     */
    public double getValue(){
    	return this.value;
    }
    /** 
     * method for checking if the Object in parameter has the same value. 
     * @param obj is the Object that we want to check with
     * @return false if obj is null
     * @return false if obj is a different object with this
     * @return ture is the value of this and obj is the same
     */
    public boolean equals(Object obj) {
    	     if (obj == null) return false;
    	     if ( obj.getClass() != this.getClass() )
    	          return false;
    	     Coin other = (Coin) obj;
    	     if ( this.value==other.value  )
    	          return true;
    	     return false;
    }
    /** 
     * method for checking if the coin in parameter less than, more than or equal value.
     * @param other is Coin for compare with.
     * @return -1 if other more than this.
     * @return 1 if other less than this.
     * @return 0 if other and this is the same value.
     */
    public int compareTo(Coin other){
    	if(other.equals(null)){
    		return 1;
    	}
    	if(this.value < other.value){
    		return -1;
    	}
    	else if(this.value > other.value){
    		return 1;
    	} 
    	else{
    	return 0;
    	}
    }
    public String toString(){
    	return String.format("%.1f",this.value);
    }
}

