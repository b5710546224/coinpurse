package coinpurse; 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 */
public class Main {

    /**
     * @param args not used
     */
    public static void main( String[] args ) {
    	Purse myPurse = new Purse(10);
    	ConsoleDialog ui = new ConsoleDialog(myPurse);
    	ui.run();
    	

        // 2. create a ConsoleDialog with a reference to the Purse object

        // 3. run() the ConsoleDialog

    }
}
